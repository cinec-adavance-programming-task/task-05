

public class dataSet implements Comparable<dataSet>{
	
	private final String carBrand,carModel,mYear,engineType;
	private final int engineCC,engineHorsePower;
	private final double zeroToSixtyTime;
	
	
	public dataSet(String carBrand,String carModel,String mYear,String engineType,int enginecc ,int enginehorsePower,double zeroToSixtyTime ) {
		
		this.carBrand = carBrand;
		this.carModel = carModel;
		this.mYear = mYear;
		this.engineType = engineType;
		this.engineCC = enginecc;
		this.engineHorsePower = enginehorsePower;
		this.zeroToSixtyTime = zeroToSixtyTime;
		
	}
	
	public String getcarBrand() {
		 return carBrand;
	}
	
	public String getcarModel() {
		 return carModel;
	}
	
	public String getMYear() {
		 return mYear;
	}
	
	public String getEngineType() {
		 return engineType;
	}
	
	public int getEngineCC() {
		 return engineCC;	
	}
	
	public int getEngineHorsePower() {
		 return engineHorsePower;
	}
	
	public double getZerotoSixtyTime() {
		 return zeroToSixtyTime;
	}
	
	public String toString() {
		 return String.format("\n\n\"The car brand : %s\n\"Model is : %s\n\"Manufature year : %s\n\"Engine type : %s \n\"Engine CC : %d \n\"Horse Power : %d \n\"0-60mph : %2f\n\n",carBrand,carModel,mYear,engineType,engineCC,engineHorsePower,zeroToSixtyTime);
	}
	
	public int compareTo(dataSet carList) {
		 return ((Integer) engineHorsePower).compareTo(carList.engineHorsePower);
	}
	public int compareToDouble(dataSet carList) {
	     return ((Double) zeroToSixtyTime).compareTo(carList.zeroToSixtyTime);
}
	

}
