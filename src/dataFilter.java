import java.util.LinkedList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;


public class dataFilter {

	public static List<dataSet> fliterByModelName(List<dataSet> cars, String modelName){
		List<dataSet> carDetails = new LinkedList<dataSet>();
		for (dataSet carList : cars) {
           	if (carList.getcarBrand().contains(modelName)) {
				carDetails.add(carList);
			}else if(carList.getcarModel().contains(modelName)) {
				carDetails.add(carList);
			}else if(carList.getMYear().contains(modelName)){
				carDetails.add(carList);
			}else if(carList.getEngineType().contains(modelName)) {
				carDetails.add(carList);
			}
		}
		return carDetails;
		
	}
	public static List<dataSet>filterWithParallelStream(List<dataSet> cars, String modelName){
		return cars.parallelStream()
				.filter(
						car ->  car.getcarBrand().startsWith(modelName)||
						car.getcarModel().endsWith(modelName) ||
						car.getMYear().endsWith(modelName) ||
						car.getEngineType().contains(modelName) ||
						car.getcarBrand().matches(modelName)
						
						)
				.collect(Collectors.toList());
	
	}
	public static List<dataSet>filterWithSequentialStream(List<dataSet> cars, String modelName){
		return cars.stream()
				.filter(
						car -> 
						car.getcarBrand().startsWith(modelName)||
						car.getcarModel().endsWith(modelName) ||
						car.getMYear().endsWith(modelName) ||
						car.getEngineType().contains(modelName) ||
						car.getcarBrand().matches(modelName)
						
						)
				.collect(Collectors.toList());
	
	} 
	
	public static List<dataSet> sequentialStreamDemo() {
		setData datas = new setData();
		List<dataSet> cars = datas.dataSet;
		cars.stream().sorted();
		return null;
		
	}
	
	public static List<dataSet> parallelStreamDemoWithSort() {
		setData datas = new setData();
		List<dataSet> cars = datas.dataSet;
		cars.parallelStream().sorted();
		return null;
		
	}
	
	public static List<dataSet>parallelStreamDemo() {
		setData datas = new setData();
		List<dataSet> cars = datas.dataSet;
		cars.parallelStream().sorted((c1, c2) -> 
        ((Integer) c1.getEngineHorsePower()).compareTo(c2.getEngineHorsePower()));
		return cars;
		
	}
	
	public static List<dataSet>parallelStreamDemoWithDoubleValue() {
		setData datas = new setData();
	    List<dataSet> cars = datas.dataSet;

	    List<dataSet> sortedCars = cars.parallelStream()
	            .sorted((c1, c2) -> Double.compare(c1.getZerotoSixtyTime(), c2.getZerotoSixtyTime()))
	            .collect(Collectors.toList());

	    return sortedCars;
		
	}
	public static List<dataSet> getByEngincc(int engineCapacity){
		setData datas = new setData();
		List<dataSet> cars = datas.dataSet;
		System.out.print("cars with " +engineCapacity+" engine cc");
		cars.stream()
		.filter(carModel -> carModel.getEngineCC() == engineCapacity )
		.forEach(System.out::println);
		return cars;
		
		
	}
	
	public static List<dataSet> findMinAndMaxHorsePower(){
		setData datas = new setData();
		List<dataSet> cars = datas.dataSet;
		 OptionalInt min = cars.stream().mapToInt(dataSet::getEngineHorsePower).min();
		 if (min.isPresent()) {
		      System.out.printf("Lowest Horse Power car is %d\n", min.getAsInt());
		    } else {
		      System.out.println("min failed");
		    }
		return cars;
		
		
	}
	
	public static List<dataSet> parallelandSequentialStream(){
		setData datas = new setData();
		List<dataSet> cars = datas.dataSet;
		cars.stream().forEach(x -> System.out.println("sequential" +x));
		System.out.println();
		cars.parallelStream().forEach(System.out::println);
		return null;
		
	}
	
}
